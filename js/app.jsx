

/* global React */
var Header = React.createClass({
  render: function () {
    return (
    	<div>
            <div className="col-sm-12 header-class">
      	        <p className="header-content">This is the header</p>
            </div>
        </div>
    );
  }
});

/* global ReactDOM */
ReactDOM.render(
  <Header />,
  document.getElementById('header')
);


/* global React */
var Navigation = React.createClass({
  render: function () {
    return (
    	<div>
            <div className="col-sm-12 nav-class">
      	        <p className="nav-content">This is the navigation bar</p>
            </div>
        </div>
    );
  }
});

/* global ReactDOM */
ReactDOM.render(
  <Navigation />,
  document.getElementById('nav')
);